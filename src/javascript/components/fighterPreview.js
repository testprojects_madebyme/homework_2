import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  function createProperty(keyValue) {
    const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___property' });
    nameElement.innerText = keyValue.join(': ').replace(/(\w+):/, subStr => subStr.toUpperCase());
  
    return nameElement;
  }

  function createPreviewImage(source) {
    const attributes = { src: source };
    const imgElement = createElement({
      tagName: 'img',
      className: 'fighter-image___preview',
      attributes
    });

    if(position === 'right') {
      imgElement.style.transform = 'scale(-1, 1)';
    }
  
    return imgElement;
  }

  if(fighter) {
    const keyValueArrayOfObject = Object.entries(fighter);
    fighterElement.append(createPreviewImage(fighter['source']));
    keyValueArrayOfObject
    .filter(keyValueAll => keyValueAll[0] !== '_id' && keyValueAll[0] !== 'source')
    .forEach(keyValue => fighterElement.append(createProperty(keyValue)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterData(fParam,fName,fData) {
  const dataDiv   = createElement({
    tagName: "div",
    className: "fighter-show__data"
  });
  const dataImg   = createElement({
    tagName: "img",
    className: "fighter-show__data_img",
    attributes: { "src": "Img/"+fParam.toLowerCase()+".png", alt: "info image"} 
  });
  const dataLabel = createElement({
    tagName: "label",
    className: "fighter-show__data_label"
  }); 

  dataLabel.innerText = fName+"  =>  "+fData;
  dataDiv.append(dataImg,dataLabel);
  return dataDiv;
}

function removeFighterInfo(arena,position) {
  const id = `fighter-show__${position}`;

  let oldInfo = document.getElementById(id);
  if (oldInfo) {
    arena.removeChild(oldInfo);
    oldInfo.id = "";
  }
}