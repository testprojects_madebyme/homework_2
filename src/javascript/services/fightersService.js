import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  // async getFighterDetails(id) {
  //   try {
  //     const endpoint = `/${id}`;
  //     const fighter = await callApi(endpoint, 'GET');
  //     this.current = this.selectLast ? 1 : 0;
  //     this.currentSelection = FighterService.posPreview[this.current];
  //     this.selectLast = !this.selectLast;
  //     return fighter;
  //   } catch (error) {
  //     alert("Error search fighter "+id+" !");
  //   };
  // }

  // async getFighterDetails(id) {
  //   // todo: implement this method
  //   // endpoint - `details/fighter/${id}.json`;
  // }

}

export const fighterService = new FighterService();
